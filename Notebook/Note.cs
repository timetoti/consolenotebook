﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Notebook
{
    public class Note
    {
        public string lastname, name, patronymic, number, country, org, prof, inf, bd;
        public static int k = 0;

        public Note(string lastname, string name, string number, string country, DateTime date, string patronymic = "", string org = "", string prof = "", string inf = "")
        {
            this.lastname = lastname;
            this.name = name;
            this.patronymic = patronymic;
            this.number = number;
            this.country = country;
            this.org = org == "" ? "-" : org;
            this.prof = prof == "" ? "-" : prof;
            this.inf = inf == "" ? "-" : inf;
            this.bd = date == DateTime.Today ? "-" : date.ToString().Remove(10);
        }

        public string Show()
        {
            return "ФИО: " + lastname + ' ' + name + ' ' + patronymic + "\r\n" +
                "Телефонный номер: " + number + "\r\n" +
                "Страна проживания: " + country + "\r\n" +
                "Дата рождения: " + bd + "\r\n" +
                "Организация: " + org + "\r\n" +
                "Должность: " + prof + "\r\n" +
                "Примечание: " + inf;
        }

        public override string ToString()
        {
            return lastname + ' ' + name + ' ' + number;
        }
    }
}
