﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Notebook
{
    class Notebook
    {
        static Dictionary<int, Note> notes = new Dictionary<int, Note>();

        static void Main(string[] args)
        {

            Console.WriteLine("Привет пользователь! На данный момент твоя записная книжка ты пуста!");
            Console.WriteLine();
            Console.WriteLine("Нажмите любую клавишу для добавления новой записи");
            Console.ReadKey();
            notes.Add(++Note.k, Createnew());

            while (true)
            {
                Update();
                string choice = Console.ReadLine();

                if (choice == "Выход")
                {
                    break;
                }

                if (Checkspelling(choice))
                {
                    switch (choice[0])
                    {
                        case '0':
                            notes.Add(++Note.k, Createnew());
                            break;
                        case 'п':
                            Console.Clear();
                            Console.WriteLine("Просмотр записи, для возврата в меню нажмите любую клавишу");
                            Console.WriteLine();
                            Console.WriteLine(notes[Int32.Parse(choice.Remove(0, 1))].Show());
                            Console.ReadKey();
                            break;
                        case 'р':
                            int num = Int32.Parse(choice.Remove(0, 1));
                            Note n = Editnote(num);
                            notes.Remove(num);
                            notes.Add(num, n);
                            break;
                        case 'у':
                            Deletenote(Int32.Parse(choice.Remove(0, 1)));
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Ошибочнная команда, нажмите любую клавишу для продолжения и попробуйте снова");
                    Console.ReadKey();
                }
            }
        }

        public static void Update()
        {
            Console.Clear();
            Console.WriteLine("Вы находитесь в главном меню записной книжки.");
            Console.WriteLine("Для создания новой записи введите цифру '0' и нажмите Enter.");
            Console.WriteLine("Для просмотра записи введите букву 'п' и число, соответствующее id данной записи, и нажмите Enter.");
            Console.WriteLine("Для редактирования записи введите букву 'р' и число, соответствующее id данной записи, и нажмите Enter.");
            Console.WriteLine("Для удаления записи введите букву 'у' и число, соответствующее id данной записи, и нажмите Enter.");
            Console.WriteLine("Id записи указывается в фигурных скобках перед самой записью.");
            Console.WriteLine("Для выхода из программы введите \"Выход\" и нажмите Enter.");
            Console.WriteLine();
            Console.WriteLine(notes.Count != 0 ? "Текущий список контактов:" : "На данный момент нет ни одного контакта");

            for (int i = 1; i <= Note.k; i++)
            {
                if (notes.ContainsKey(i))
                {
                    Console.WriteLine();
                    Console.WriteLine('{' + i.ToString() + "} " + notes[i]);
                }
            }
        }

        public static Note Createnew()
        {
            string lastname, name, number, country, patronymic, org, prof, inf;
            DateTime date;

            Console.Clear();
            Console.WriteLine("Создание нового контакта");
            Console.WriteLine();

            while (true)
            {
                Console.WriteLine("Введите фамилию нового контакта: ");
                lastname = Console.ReadLine();
                if (lastname != "")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Ошибка! Неверный формат фамилии!");
                    Console.WriteLine();
                }
            }

            while (true)
            {
                Console.WriteLine("Введите имя нового контакта: ");
                name = Console.ReadLine();
                if (name != "")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Ошибка! Неверный формат имени!");
                    Console.WriteLine();
                }
            }

            Console.WriteLine("Введите отчество нового контакта (необязательно, для пропуска нажмите Enter): ");
            patronymic = Console.ReadLine();

            while (true)
            {
                Console.WriteLine("Введите номер телефона нового контакта: ");
                string str = Console.ReadLine();
                Regex regex = new Regex(@"^\+?[0-9]{5,}$");
                if (regex.IsMatch(str))
                {
                    number = str;
                    break;
                }
                else
                {
                    Console.WriteLine("Ошибка! Номер телефона должен состоять только из цифр, может содеражать знак '+' в начале и должен быть не короче 5 символов");
                    Console.WriteLine();
                }
            }

            while (true)
            {
                Console.WriteLine("Введите страну проживания нового контакта: ");
                country = Console.ReadLine();
                if (country != "")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Ошибка! Неверный формат страны!");
                    Console.WriteLine();
                }
            }

            while (true)
            {
                Console.WriteLine("Введите дату рождения нового контакта в формате ДД.ММ.ГГГГ (необязательно, для пропуска нажмите Enter): ");
                string str = Console.ReadLine();
                if (str == "")
                {
                    date = DateTime.Today;
                    break;
                }
                DateTime dt = DateTime.Today;
                if (DateTime.TryParse(str, out dt))
                {
                    date = dt;
                    break;
                }
                else
                {
                    Console.WriteLine("Ошибка! Неверный формат даты");
                    Console.WriteLine();
                }
            }

            Console.WriteLine("Введите организацию нового контакта (необязательно, для пропуска нажмите Enter): ");
            org = Console.ReadLine();

            Console.WriteLine("Введите должность нового контакта (необязательно, для пропуска нажмите Enter): ");
            prof = Console.ReadLine();

            Console.WriteLine("Введите дополнительную информацию нового контакта (необязательно, для пропуска нажмите Enter): ");
            inf = Console.ReadLine();

            return new Note(lastname, name, number, country, date, patronymic, org, prof, inf);
        }

        public static bool Checkspelling(string str)
        {
            if (str != "")
            {
                if (str[0] == '0')
                {
                    return true;
                }
                else
                {
                    if (str[0] == 'п' || str[0] == 'р' || str[0] == 'у')
                    {
                        str = str.Remove(0, 1);
                        return Int32.TryParse(str, out int i) ? notes.ContainsKey(i) : false;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }

        public static Note Editnote(int num)
        {
            string lastname, name, number, country, patronymic, org, prof, inf;
            DateTime date;

            Console.Clear();
            Console.WriteLine("Редактирование записи");
            Console.WriteLine();

            Console.WriteLine("Текущая фамилия контакта: " + notes[num].lastname);
            Console.WriteLine("Введите новую фамилию контакта или нажмите Enter для оставления данного поля без изменений");
            string str = Console.ReadLine();
            lastname = str == "" ? notes[num].lastname : str;

            Console.WriteLine();
            Console.WriteLine("Текущее имя контакта: " + notes[num].name);
            Console.WriteLine("Введите новое имя контакта или нажмите Enter для оставления данного поля без изменений");
            str = Console.ReadLine();
            name = str == "" ? notes[num].name : str;

            Console.WriteLine();
            Console.WriteLine("Текущее отчество контакта : " + notes[num].patronymic);
            Console.WriteLine("Введите новое отчество контакта или нажмите Enter для оставления данного поля без изменений");
            str = Console.ReadLine();
            patronymic = str == "" ? notes[num].patronymic : str;

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Текущий телефонный номер контакта: " + notes[num].number);
                Console.WriteLine("Введите новый телефонный номер контакта или нажмите Enter для оставления данного поля без изменений");
                str = Console.ReadLine();
                if (str == "")
                {
                    number = notes[num].number;
                    break;
                }
                else
                {
                    Regex regex = new Regex(@"^\+?[0-9]{5,}$");
                    if (regex.IsMatch(str))
                    {
                        number = str;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Ошибка! Телефонный номер должен состоять только из цифр, может содержать знак '+' в начале и быть не короче 5 символов");
                        Console.WriteLine("Нажмите любую клавишу для продолжения");
                        Console.ReadKey();
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine("Текущая страна контакта: " + notes[num].country);
            Console.WriteLine("Введите новую страну контакта или нажмите Enter для оставления данного поля без изменений");
            str = Console.ReadLine();
            country = str == "" ? notes[num].country : str;

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Текущая дата рождения контакта: " + notes[num].country);
                Console.WriteLine("Введите новую дату рождения контакта в формате ДД.ММ.ГГГГ или нажмите Enter для оставления данного поля без изменений");
                str = Console.ReadLine();
                if (str == "")
                {
                    date = DateTime.Parse(notes[num].bd);
                    break;
                }
                else
                {
                    DateTime dt = DateTime.Today;
                    if (DateTime.TryParse(str, out dt))
                    {
                        date = dt;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Неправильный формат даты");
                        Console.WriteLine("Нажмите клавишу для продолжения");
                        Console.ReadKey();
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine("Текущая организация контакта: " + notes[num].org);
            Console.WriteLine("Введите новую организацию контакта или нажмите Enter для оставления данного поля без изменений");
            str = Console.ReadLine();
            org = str == "" ? notes[num].org : str;

            Console.WriteLine();
            Console.WriteLine("Текущая должность контакта: " + notes[num].prof);
            Console.WriteLine("Введите новую должность контакта или нажмите Enter для оставления данного поля без изменений");
            str = Console.ReadLine();
            prof = str == "" ? notes[num].prof : str;

            Console.WriteLine();
            Console.WriteLine("Текущее примечание о контакте: " + notes[num].inf);
            Console.WriteLine("Введите новое примечание о контакте или нажмите Enter для оставления данного поля без изменений");
            str = Console.ReadLine();
            inf = str == "" ? notes[num].inf : str;

            return new Note(lastname, name, number, country, date, patronymic, org, prof, inf);
        }

        public static void Deletenote(int num)
        {
            Console.Clear();
            Console.WriteLine("Вы действительно хотите удалить запись " + notes[num] + '?');
            Console.WriteLine("Нажмите клваишу 'Y' для подтверждения удаления, или любую другую клавишу для отмены удаления и возврата в главное меню");
            ConsoleKeyInfo cki = Console.ReadKey();
            if (cki.Key == ConsoleKey.Y)
            {
                notes.Remove(num);
            }
        }
    }
}